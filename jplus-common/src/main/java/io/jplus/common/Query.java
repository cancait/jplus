/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.jplus.common;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jplus.xss.SQLFilter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 查询参数
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0 2017-03-14
 */
public class Query<T> extends LinkedHashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    /**
     * mybatis-plus分页参数
     */
    private Page<T> page;
    /**
     * 当前页码
     */
    private int currPage = 1;
    /**
     * 每页条数
     */
    private int limit = 10;

    private String orderBy;

    public Query() {
        //作为参数必需有一个无参的构造方法

    }

    public Query(Map<String, Object> params) {

        for (String key : params.keySet()) {
            String[] values = (String[]) params.get(key);
            String value = StrKit.join(values, ",");
            this.put(key, value);
        }

        //分页参数
        if (params.get("page") != null) {
            String pages = (String) this.get("page");
            currPage = Integer.parseInt(pages);
        }
        if (params.get("limit") != null) {
            String limits = (String) this.get("limit");
            limit = Integer.parseInt(limits);
        }

        this.put("offset", (currPage - 1) * limit);
        this.put("page", currPage);
        this.put("limit", limit);

        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String sidx = SQLFilter.sqlInject((String) this.get("sidx"));
        String order = SQLFilter.sqlInject((String) this.get("order"));
        this.put("sidx", sidx);
        this.put("order", order);

        if (StrKit.notBlank(sidx)) {
            orderBy = sidx;
            if (StrKit.notBlank(order)) {
                orderBy = orderBy + " " + order;
            }
        }

    }

    public Page<T> getPage() {
        return page;
    }

    public int getCurrPage() {
        return currPage;
    }

    public int getLimit() {
        return limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

}
