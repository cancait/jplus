package io.jplus.admin.service.impl;

import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.ScheduleJobLog;
import io.jplus.admin.service.ScheduleJobLogService;


@Bean
@RPCBean
public class ScheduleJobLogServiceImpl extends JbootServiceBase<ScheduleJobLog> implements ScheduleJobLogService {

}