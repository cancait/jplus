/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.db.model.Columns;
import io.jboot.utils.StrUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.annotation.JplusLog;
import io.jplus.admin.model.Menu;
import io.jplus.admin.service.MenuService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;

import java.util.List;

@RequestMapping(value = "/admin/menu", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class MenuController extends BaseController {

    @RPCInject
    MenuService menuService;

    public void index() {
        render("menu.html");
    }

    @RequiresUser
    public void nav() {
        List<Menu> menuList = menuService.findListByUserId(getUserId());
        renderJson(JSONObject.toJSONString(Ret.ok("menuList", menuList)));
    }

    @RequiresPermissions("sys:menu:list")
    public void list() {
        String menuName = getPara("name");
        Columns columns = Columns.create();
        if (StrKit.notBlank(menuName)) {
            columns.like("name", menuName);
        }
        List<Menu> menuList = menuService.findByColumns(columns);
        renderJson(JsonKit.toJson(menuList));
    }

    @RequiresPermissions("sys:menu:info")
    public void info() {
        String menuId = getPara();
        Menu menu = menuService.findMenuById(menuId);
        renderJson(Ret.ok("menu", menu));
    }


    @RequiresPermissions("sys:menu:select")
    public void select() {
        List<Menu> menuList = menuService.findNotButtonList();
        Menu root = new Menu();
        root.setId("0");
        root.setName("一级菜单");
        root.setPid("-1");
        root.setOpen(true);
        menuList.add(root);
        renderJson(Ret.ok("menuList", menuList));
    }


    @JplusLog("删除菜单")
    @RequiresPermissions("sys:menu:delete")
    public void delete(String menuId) {
        if (menuId==null) {
            renderJsonForFail("Id不能为空");
            return;
        }
        boolean tag = menuService.deleteById(menuId);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("删除失败!");
        }
    }


    @JplusLog("保存菜单")
    @Before(POST.class)
    @RequiresPermissions("sys:menu:save")
    public void save() {
        Menu menu = jsonToModel(Menu.class);
        String id = (String) menuService.save(menu);
        if (StrUtil.isNotBlank(id)) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("保存失败!");
        }
    }

    @JplusLog("更新菜单")
    @Before(POST.class)
    @RequiresPermissions("sys:menu:update")
    public void update() {
        Menu menu = jsonToModel(Menu.class);
        if (menuService.update(menu)) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("更新失败!");
        }
    }

}
